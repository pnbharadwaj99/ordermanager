# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import decimal

from django.db import models

CHOICE_CUSTOMER_TYPE = (
    ('we','weekly'),
    ('mo','montly'),
)

CHOICE_ITEM_TYPE = (
    ('D','D'),
    ('W','W'),
    ('Loop','LOOP'),
    ('Box','BOX'),
    ('Custom','CUSTOM'),
)

CHOICE_ITEM_QTY_TYPE = (
    ('kg','kg'),
    ('pc','pc'),
)

# Create your models here.
class Customer(models.Model):
    customer_name = models.CharField(max_length=200)
    customer_mobile = models.CharField(max_length=12)
    customer_alt_number = models.CharField(max_length=15,blank=True,null=True)
    customer_gst = models.CharField("Customer GSTIN",max_length=40,null=True,blank=True)
    customer_type = models.CharField(max_length=20,choices=CHOICE_CUSTOMER_TYPE)
    customer_city = models.CharField(max_length=50)
    customer_landmark = models.CharField(max_length=200,null=True,blank=True)
    customer_comments = models.CharField(max_length=500,null=True,blank=True)
    customer_is_active = models.BooleanField(default=True)
    def __str__(self):
        return self.customer_name


class Order(models.Model):
    
    order_customer = models.ForeignKey(Customer,models.CASCADE)
    order_date = models.DateField(auto_now_add=True)
    order_delivery_date = models.DateField(blank=True,null=True)
    order_is_delivered = models.BooleanField(default=False)
    order_payment = models.BooleanField(default=False)
    order_comment = models.TextField(blank=True,null=True)
    
    @property
    def order_total(self):
        sum = 0
        items = self.item_set.all()  
        for item in items:
            sum+=item.total
        return sum

    @property
    def grand_total(self):
        
        sum = self.order_total
        sum = sum+self.get_cgst+self.get_sgst
        return round(sum, 2)
    
    @property
    def get_cgst(self):
        try:
            tax = Tax.objects.get(pk=1)
            invoice = self.invoice
            cgst = invoice.invoice_cgst
            if(cgst is None ):
                cgst = tax.cgst
        except Invoice.DoesNotExist:
            cgst = tax.cgst
        return self.order_total*cgst*decimal.Decimal(0.01)

    @property
    def get_sgst(self):
        try:
            tax = Tax.objects.get(pk=1)
            invoice = self.invoice
            sgst = invoice.invoice_sgst
            if(sgst is None ):
                sgst = tax.sgst
        except Invoice.DoesNotExist:
            sgst = tax.sgst
        return self.order_total*sgst*decimal.Decimal(0.01)
    def __str__(self):
        return str(self.id)

class Item(models.Model):
    item_order = models.ForeignKey(Order,models.CASCADE)
    item_type = models.CharField(max_length=10, choices=CHOICE_ITEM_TYPE)
    item_size = models.CharField(max_length=10)
    item_color = models.CharField(max_length=20)
    item_gsm = models.CharField(max_length=10)
    item_rate = models.DecimalField(max_digits=6,decimal_places=2)
    item_quantity = models.DecimalField(max_digits=8, decimal_places=2)
    item_quantity_type = models.CharField(max_length=10, choices=CHOICE_ITEM_QTY_TYPE)

    @property
    def total(self):
        if(self.item_rate==None or self.item_quantity==None):
            return 0
        return self.item_rate * self.item_quantity


    def __str__(self):
        return self.item_type + '-' + self.item_size

class Invoice(models.Model):
    invoice_order = models.OneToOneField(Order,on_delete=models.CASCADE)
    invoice_is_payed = models.BooleanField(default=False)
    invoice_date =  models.DateField(auto_now=True)
    invoice_sgst = models.DecimalField(max_digits=6,decimal_places=2,blank=True,null=True)
    invoice_cgst = models.DecimalField(max_digits=6,decimal_places=2,blank=True,null=True)
    def __str__(self):
        return str(self.id)

class Tax(models.Model):
    class Meta:
        verbose_name_plural = "Taxes"
    sgst = models.DecimalField(max_digits=6,decimal_places=2,blank=True,null=True)
    cgst = models.DecimalField(max_digits=6,decimal_places=2,blank=True,null=True)
    
    def __str__(self):
        return "SGST : " + str(self.sgst) + ",    CGST : " + str(self.cgst)




class SaleSummary(Order):
    class Meta:
        proxy = True
        verbose_name = "Sale Summary"
        verbose_name_plural = "Sales Summary"
