# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import calendar
from datetime import date

from django import forms
from django.conf.urls import url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Count, Sum
from django.forms import ModelForm, Textarea, TextInput
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from .models import Customer, Invoice, Item, Order, SaleSummary, Tax

admin.site.site_header = "Canara Eco Bags"
# Register your models here.


class MonthListFilter(admin.DateFieldListFilter):
    title = _('Month')

    def __init__(self, field, request, params, model, model_admin, field_path):
        self.lookup_kwarg = '%s__month' % field_path
        self.lookup_val = request.GET.get(self.lookup_kwarg)
        super(MonthListFilter, self).__init__(
            field, request, params, model, model_admin, field_path)

    def expected_parameters(self):
        return [self.lookup_kwarg]

    def choices(self, cl):
        yield {
            'selected': self.lookup_val is None,
            'query_string': cl.get_query_string({}, [self.field.name]),
            'display': _('All'),
        }

        for month in range(1, 13):
            yield {
                'selected': self.lookup_val == str(month),
                'query_string': cl.get_query_string(
                    {'{}__month'.format(self.field.name): month},
                    [self.field.name]),
                'display': _(calendar.month_name[month])
            }


class CustomerModelAdmin(admin.ModelAdmin):
    list_display = ['customer_name', 'customer_mobile', 'customer_city']
    list_filter = ['customer_type', 'customer_is_active']
    search_fields = ['customer_name','customer_city']

    class Meta:
        model = Customer


class ItemLine(admin.TabularInline):
    model = Item
    extra = 1
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
    }
    readonly_fields = ['total']


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = '__all__'

    def clean_order_delivery_date(self):
        order_date = self.instance.order_date
        delivery_date = self.cleaned_data.get('order_delivery_date')
        if(order_date is None):
            order_date = date.today()
        if(delivery_date is not None and delivery_date < order_date):
            raise forms.ValidationError(
                "Delivery Date must be Greater/Equal to Order Date(" + str(order_date) + ")")
        return delivery_date


class OrderModelAdmin(admin.ModelAdmin):
    form = OrderForm
    list_display = [
        'id', 'order_customer',
        'order_date', 'order_delivery_date',
        'order_is_delivered', 'order_payment', 'grand_total',
        'get_invoice',
    ]
    
    list_editable = ['order_is_delivered', 'order_payment']
    list_filter = ('order_is_delivered', 'order_payment',
                   ('order_date', MonthListFilter))
    inlines = (ItemLine,)
    readonly_fields = ['grand_total', ]
    search_fields = ['id', 'order_customer__customer_name']
    ordering = ['-id']

    class Meta:
        model = Order

    # method to display sorted list of customer list on create order page
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "order_customer":
            kwargs["queryset"] = Customer.objects.order_by('customer_name')
        return super(OrderModelAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_urls(self):
        urls = super(OrderModelAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<order_id>[0-9]+)/manager_invoice_change$',
                self.admin_site.admin_view(self.show_invoice),
                name='invoice_create',
            )]
        return urls + custom_urls

    def get_invoice(self, obj):
        button_name = "Show Invoice"
        try:
            obj.invoice
        except Invoice.DoesNotExist:
            button_name = "Generate Invoice"

        return mark_safe(format_html(
            '<a class="button" target="_blank" href="{}">{}</a>&nbsp;',
            reverse('admin:invoice_create', args=(obj.pk,)), button_name),
        )
    get_invoice.short_description = 'Invoice'
    get_invoice.allow_tags = True

    def show_invoice(self, request, order_id, *args, **kwargs):
        order = Order.objects.get(pk=order_id)
        try:
            o = order.invoice
        except Invoice.DoesNotExist:
            tax = Tax.objects.get(pk=1)
            cgst = tax.cgst
            sgst = tax.sgst
            order.invoice = Invoice(invoice_is_payed=True,invoice_cgst=cgst,invoice_sgst=sgst)
            order.invoice.save()

        loop_counter = range(order.item_set.count(), 9)
        return TemplateResponse(
            request,
            'admin/manager/invoice.html',
            {'order': order, 'loop_counter': loop_counter}
        )

    

class TaxAdmin(admin.ModelAdmin):
    class Meta:
        model = Tax


class SaleSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/manager/stats.html'
    date_hierarchy = 'order_date'
    list_filter = ['order_payment']
    def has_add_permission(self, request):
        return False


    def changelist_view(self, request, extra_context=None):
        response = super(SaleSummaryAdmin,self).changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data["cl"].queryset
        except (AttributeError, KeyError):
            return response
        


        response.context_data["summary"] = list(
            qs.all()
        )

        response.context_data["summary_total"] = {
            "Count":qs.count(),
            "Total":sum(order.grand_total for order in qs.all())
        }
        
        return response

admin.site.register(Customer, CustomerModelAdmin)
admin.site.register(Order, OrderModelAdmin)
admin.site.register(Tax,TaxAdmin)
admin.site.register(SaleSummary,SaleSummaryAdmin)
